#!/bin/dash

# This script is to be pasted into a Stack Script on Linode.
# For the OS, check Centos 6.8.

# Centos 6 comes with an "adm" user; Ubuntu does not.
# Found that "adm" did not own any files.
# Password file entry as comes:
# adm:x:3:4:adm:/var/adm:/sbin/nologin
# /var/adm did not exist.

cd
exec >stack_script_log 2>&1
set -ux
set -ux
set -e
: Welcome to $0.

yum -y update
yum install -y git gcc-c++

chfn -f "System Administration" adm
usermod --home /home/adm --shell /bin/bash  adm
umask 77 # other users need access to adm's home dir for doing user admin.
mkdir -p /home/adm
chown adm /home/adm
chgrp adm /home/adm
cp -Ra .ssh /home/adm
chown adm /home/adm/.ssh /home/adm/.ssh/*
chgrp adm /home/adm/.ssh /home/adm/.ssh/*

su -s /bin/dash -l adm <<'!'
  exec >root_log 2>&1
  set -ux
  set -ux
  set -e
  pwd
  id
  umask
  ls -la
  : add here if this much works.
  echo done
!

: $0 done

