#!/bin/dash

# This script is to be pasted into a Stack Script on Linode.
# For the OS, check Centos 6.8.

cd
exec 2>&1 >stack_script_log
set -x
set -u
set -e # unless testing.

# Centos 6 comes with an "adm" user; Ubuntu does not.
# Found that "adm" did not own any files.
# Password file entry as comes:
# adm:x:3:4:adm:/var/adm:/sbin/nologin
# /var/adm did not exist.

yum -y update
yum install -y git gcc-c++

chfn -f "System Administration" adm
usermod --home /home/adm --shell /bin/bash  adm
umask 77
mkdir -p /home/adm
chown adm /home/adm
chgrp adm /home/adm
chmod 755 /home/adm # other users need access for doing user admin.
umask 2
cat <<'!' >/tmp/root_to_adm.sh
  echo Welcome to $0.
  set -x
  set -u
  set -e # Omit this when testing piecemeal.

  umask 77
  mkdir -p .ssh
  cd .ssh
  umask 33
  path1=https://bitbucket.org/jack_waugh/srv_init_00/raw
  path2=1491c42e8319a82f4781f343c53c787e6dac7598
  path3=skel/ssh
  for basename in known_hosts authorized_keys
  do
    curl -o $basename $path1/$path2/$path3/$basename 2>/dev/null
  done
  chmod 600 authorized_keys # Necessary on Centos 6? Not on Ubuntu.
  
  cd

  umask 2
  chmod 775 .
  mkdir -p projects
  cd projects
  # git clone https://jack_waugh@bitbucket.org/jack_waugh/srv_init_00.git
  cd

  echo $0 done
!

su -s /bin/dash -l -c "dash /tmp/root_to_adm.sh" adm

# rm /tmp/root_to_adm.sh
echo $0 done

